const flickity				= require('flickity'),
			objectFitImages = require('object-fit-images'),
			Parallax 				= require('parallax-js');

objectFitImages();

const hero_slider = document.querySelector('.hero-slider');

if (hero_slider) {
	const partners_slider = new flickity( hero_slider, {
		cellAlign: 'left',
		contain: true,
		prevNextButtons:false,
		adaptiveHeight: false,
		setGallerySize: false,
		pageDots: 			true,
		wrapAround: 		true,
		arrowShape: { 
		  x0: 10,
		  x1: 60, y1: 50,
		  x2: 65, y2: 45,
		  x3: 20
		},
		cellSelector: '.slide'
	});
}

const interior_slider = document.querySelector('.interior-slider');
if (interior_slider) {
	const int_slider = new flickity( interior_slider, {
		cellAlign: 'left',
		contain: true,
		wrapAround: true,
		pageDots: true,
		prevNextButtons: false,
		adaptiveHeight: false,
		setGallerySize: false,
		cellSelector: '.slide'
	});
}

const scene = document.getElementById('scene');
const parallaxInstance = new Parallax(scene, {
	relativeInput: true
});

const btn = document.querySelector('.form-scroll');
if (btn) {
	btn.addEventListener( 'click', (e) => {
		e.preventDefault();
		const loc = document.getElementById( 'form' ).offsetTop;
		
		if ( "scrollBehavior" in document.documentElement.style ) {
			window.scrollTo( { top: loc, behavior: "smooth"} );
		} else {
			window.scroll( 0, loc );
		}
	});
}


